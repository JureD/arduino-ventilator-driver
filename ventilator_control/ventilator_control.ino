#include <LiquidCrystal.h>


#define motorPin 9
#define sensorPin 2
#define inputPin A1

const int rs = 12, en = 11, d4 = 7, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int inputValue;
int motorSpeed;
unsigned long t;
unsigned long f;

void setup() {
  lcd.begin(16, 2);
  pinMode(motorPin, OUTPUT);
  pinMode(sensorPin, INPUT);

}

void loop() {
  inputValue = analogRead(inputPin);
  
  motorSpeed = map(inputValue, 0, 1024, 0, 255);
  analogWrite(motorPin, motorSpeed);

  t = pulseIn(sensorPin, HIGH, 500000);
  f = 1000000 / t;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Frequency [Hz]:");
  lcd.setCursor(0, 1);
  if (f > 100) {
    lcd.print("Stopped");
  } else {
    lcd.print(f);
  }

}
